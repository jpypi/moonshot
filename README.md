Moonshot
How to Install:

This game uses python 3, you will need it to play.
Download the zip folder and unzip it.
Run "pip install -r requirements.txt" within the unzipped folder or simply pip install pyglet.
Run "python main.py" within the unzipped folder.

How to play:
Mouse controls: A, D move left/right, W to jump.
Ship Controls: A, D rotate left/right, W engages thrusters, Spacebar to fire weapons
Get to the moon, but don't get lost in space!

Original Spritework by Strongth
Original Code by Mind & Strongth
SFX created on sfxr.me

Additional Sprite Credits:
Ship explosion created by Sogomn
https://opengameart.org/content/explosion-3
https://opengameart.org/users/sogomn

Music Credits:
Bensound.com provided us with the three tracks free of charge, links here:
https://www.bensound.com/royalty-free-music/track/epic
https://www.bensound.com/royalty-free-music/track/dance
https://www.bensound.com/royalty-free-music/track/summer-chill-relaxed-tropical 
