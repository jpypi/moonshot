import pyglet as _pyglet

from vector import Vector


class RectSprite(_pyglet.sprite.Sprite):
    @property
    def left(self):
        return self.x - int(self.image.anchor_x * self.scale)
    @property
    def right(self):
        return self.x - int(self.image.anchor_x * self.scale) + self.width - 1

    @property
    def top(self):
        return self.y - int(self.image.anchor_y * self.scale) + self.height - 1
    @property
    def bot(self):
        return self.y - int(self.image.anchor_y * self.scale)

    def collides_rect(self, other):
        return (self.top > other.bot and self.bot < other.top and\
                self.left < other.right and self.right > other.left)


class AnimatedMover(RectSprite):
    def __init__(self, pos, angle, batch=None, group=None):
        super().__init__(self.animation, pos[0], pos[1],
                         batch=batch, group=group)

        self.pos = Vector(pos[0], pos[1], True)
        self.vel = Vector(0, 0)

        self.angle = angle

    @property
    def angle(self):
        return self._angle

    @angle.setter
    def angle(self, value):
        self._angle = value
        self.rotation = 360 - value
        self.vel.angle = value + 90

