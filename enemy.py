import pyglet as _pyglet
import math as math

from utils import *
from base import AnimatedMover
from projectiles import EnemyBullet


def phased_bullets(phase, count, position, speed, batch=None, group=None):
    return [
            EnemyBullet(position, angle, speed, batch, group)
            for angle in range(phase, 360, 360 // count)
        ]


class Enemy(AnimatedMover):
    animation = load_animation("CatShipBoss", 5, 0.05)

    def __init__(self, pos, angle, health, batch=None, group=None):
        super().__init__(pos, angle, batch, group)

        self.bullet_speed = 450
        # Bullets per second
        self.fire_rate = 8
        # initialize this to just something bigger than rate
        self.time_since_last_bullet = 1/self.fire_rate
        self.health = health
        self.phase = 0

    def update(self, dt, keyboard, sprite_groups, environment):
        # special shots go here
        self.time_since_last_bullet += dt
        self.phase += dt
        temp_phase = round(self.phase)

        ## acutal change in behavior is relative to boss hp, which is 10k
        if self.health >= 7000:
            # 4 diagonals
            if self.time_since_last_bullet > 1 / self.fire_rate:
                self.time_since_last_bullet = 0
                return phased_bullets(45, 4, self.pos, self.bullet_speed,
                                      self.batch, sprite_groups[0])
        elif self.health >= 6000:
            # 4 diagonals, 4 cardinal
            if self.time_since_last_bullet > 1 / self.fire_rate:
                if temp_phase % 3 == 0 or temp_phase % 4 == 0:
                    self.time_since_last_bullet = 0
                    return phased_bullets(45, 4, self.pos, self.bullet_speed,
                                          self.batch, sprite_groups[0])
                else:
                    self.time_since_last_bullet = 0
                    return phased_bullets(0, 4, self.pos, self.bullet_speed,
                                          self.batch, sprite_groups[0])
        elif self.health >= 5000:
            ## 2x counter rotations, some across sideways on bottom
            if self.time_since_last_bullet > 1 / self.fire_rate:
                self.time_since_last_bullet = 0
                projectiles = (
                        EnemyBullet(self.pos, (self.phase*60)%360, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, -(self.phase*60)%360, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, ((self.phase*60)%360)+180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,100), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,200), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, -((self.phase*60)%360)+180, self.bullet_speed,
                                   self.batch, sprite_groups[0])
                    )
                return projectiles
        elif self.health >= 4000:
            ## 4x pinwheel, sideways on top
            if self.time_since_last_bullet > 1 / self.fire_rate:
                self.time_since_last_bullet = 0
                projectiles = (
                        EnemyBullet(self.pos, (self.phase*40)%360, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, (self.phase*40)%360+90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, ((self.phase*40)%360)+180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,600), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,500), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, ((self.phase*40)%360)+270, self.bullet_speed,
                                   self.batch, sprite_groups[0])
                    )
                return projectiles
        elif self.health >= 3000:
            ## 4x pinwheel opposite direction
            if self.time_since_last_bullet > 1 / self.fire_rate:
                self.time_since_last_bullet = 0
                projectiles = (
                        EnemyBullet(self.pos, (-self.phase*40)%360, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, (-self.phase*40)%360+90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, ((-self.phase*40)%360)+180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((225,700), 180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((675,700), 180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, ((-self.phase*40)%360)+270, self.bullet_speed,
                                   self.batch, sprite_groups[0])
                    )
                return projectiles
        elif self.health >= 2000:
            ## Slow Pulse Omni
            if self.time_since_last_bullet/5 > 1 / self.fire_rate:
                self.time_since_last_bullet = 0

                return phased_bullets(0, 18, self.pos, self.bullet_speed,
                                      self.batch, sprite_groups[0])
        elif self.health >= 1250:
            ## Slow Pulse Omni
            if self.time_since_last_bullet > 1 / self.fire_rate:
                self.time_since_last_bullet = 0
                projectiles = (
                        EnemyBullet((225,700), 180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((675,700), 180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,600), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,500), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,100), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,200), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                    )
                return projectiles
        elif self.health >= 500:
            ## 4x pinwheel, 4x diagonal
            if self.time_since_last_bullet > 1 / self.fire_rate:
                self.time_since_last_bullet = 0
                projectiles = (
                        EnemyBullet(self.pos, (-self.phase*40)%360, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, (-self.phase*40)%360+90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, ((-self.phase*40)%360)+180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, ((-self.phase*40)%360)+270, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, 135, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, 225, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, 45, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, 315, self.bullet_speed,
                                   self.batch, sprite_groups[0])
                    )
                return projectiles
        elif self.health >= 50:
            ## 8x pinwheel, 4 forward 4 backward
            if self.time_since_last_bullet > 1 / self.fire_rate:
                self.time_since_last_bullet = 0
                projectiles = (
                        EnemyBullet(self.pos, (-self.phase*40)%360, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, (-self.phase*40)%360+90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, ((-self.phase*40)%360)+180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, ((-self.phase*40)%360)+270, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, (self.phase*40)%360, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, (self.phase*40)%360+90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, ((self.phase*40)%360)+180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((225,700), 180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((675,700), 180, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,600), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,500), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,100), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet((0,200), -90, self.bullet_speed,
                                   self.batch, sprite_groups[0]),
                        EnemyBullet(self.pos, ((self.phase*40)%360)+270, self.bullet_speed,
                                   self.batch, sprite_groups[0])
                    )
                return projectiles
