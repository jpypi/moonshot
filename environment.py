import random

import pyglet as _pyglet

from utils import *
from base import AnimatedMover, RectSprite


__all__ = ["Diamond", "Grass", "Dirt"]


class Diamond(AnimatedMover):
    animation = load_animation("DiamondsNoSpace", 22, 0.08)


class Grass(RectSprite):
    images = load_image_grid("Ground", 4)

    def __init__(self, pos, batch=None, group=None):
        img = random.choice(self.images)
        super().__init__(img, pos[0], pos[1], batch=batch, group=group)


class Dirt(RectSprite):
    images = load_image_grid("Dirt", 4)

    def __init__(self, pos, batch=None, group=None):
        img = random.choice(self.images)
        super().__init__(img, pos[0], pos[1], batch=batch, group=group)


class Moon(RectSprite):
    image = load_centered_image("TheMoon")

    def __init__(self, pos, batch=None, group=None):
        super().__init__(self.image, pos[0], pos[1], batch=batch, group=group)

class Shuttle(RectSprite):
    image = load_centered_image("MouseShipScaffold")

    def __init__(self, pos, batch=None, group=None):
        super().__init__(self.image, pos[0], pos[1], batch=batch, group=group)
