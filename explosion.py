import pyglet as _pyglet

from utils import *
from base import AnimatedMover


class Explosion(AnimatedMover):
    animation = load_animation("explosion", 6, 0.1)
    def __init__(self, pos, angle, batch=None, group=None):
        super().__init__(pos, angle, batch, group)
        self.scale = 4
        self.explosion_sound = _pyglet.media.load('sounds/explosion.wav', streaming = False)
        self.explosion_sound.play()
        
    def on_animation_end(self):
        self.paused = True
        self.delete()
