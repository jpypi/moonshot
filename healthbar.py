import pyglet as _pyglet

from utils import load_resource


class BossBar(_pyglet.sprite.Sprite):
    # 600x16 with 2px border
    image = _pyglet.image.load(load_resource("RocketHealthBar"))
    x_offset = 19
    y_offset = 2

    def __init__(self, pos, max_val, init_val, color, batch=None, group=None):
        x = pos[0]
        y = pos[1]
        super().__init__(self.image, x, y, batch=batch, group=group)

        self.max_value = max_val

        self.bar = _pyglet.shapes.Rectangle(x + self.x_offset, y + self.y_offset,
                                            0, 12, color=color,
                                            batch=batch, group=group)

        self.full_width = 563
        self.update(init_val or max_val)

    def update(self, value):
        self.bar.width = max(value / self.max_value * self.full_width, 0)

    def setPos(self, pos):
        self.x = pos[0]
        self.y = pos[1]

        self.bar.x = self.x + self.x_offset
        self.bar.y = self.y + self.y_offset


class BasicBar:
    def __init__(self, pos, width, max_val, init_val, color, base_color, batch=None, group=None):
        self.max_value = max_val

        x, y = pos
        self.base_bar = _pyglet.shapes.Rectangle(x, y, width, 3, color=base_color,
                                                 batch=batch, group=group)
        self.bar = _pyglet.shapes.Rectangle(x, y, 0, 3, color=color,
                                            batch=batch, group=group)

        self.full_width = width
        self.update(init_val or max_val)

    def update(self, value):
        self.bar.width = max(value / self.max_value * self.full_width, 0)

    def setPos(self, pos):
        self.bar.x,self.bar.y = pos
        self.base_bar.x,self.base_bar.y = pos
