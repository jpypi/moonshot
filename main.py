#!/usr/bin/env python3

import pyglet

from scene import Scene1, Scene2, Scene3


class MainWindow(pyglet.window.Window):
    def __init__(self, size, fps=60):
        super().__init__(width=size[0], height=size[1], caption="moonshot")

        pyglet.font.add_file("fonts/Roboto-Regular.ttf")

        # Background color
        pyglet.gl.glClearColor(0.1, 0.1, 0.1, 1)

        pyglet.clock.schedule_interval(self.update, 1.0/fps)

        self.keyboard = pyglet.window.key.KeyStateHandler()
        self.push_handlers(self.keyboard)

        # Configure sound options
        pyglet.options['audio'] = ('openal', 'pulse', 'directsound', 'silent')

        # Scene handling
        self.scenes = [Scene1(self), Scene2(self), Scene3(self)]
        self.scene_index = 0
        self.curr_scene.initialize()

    @property
    def curr_scene(self):
        return self.scenes[self.scene_index]

    def on_draw(self):
        self.clear()
        self.curr_scene.sprite_batch.draw()

    def update(self, dt):
        new_scene = self.curr_scene.update(dt, self.keyboard)
        if new_scene is not None:
            if not isinstance(new_scene, int) and len(new_scene) == 2:
                index, data = new_scene
            else:
                index = new_scene
                data = None
            self.curr_scene.destroy()
            self.scene_index = index
            self.curr_scene.initialize(data)


if __name__ == "__main__":
    window = MainWindow((900, 700))
    pyglet.app.run()
