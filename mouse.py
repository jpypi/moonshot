from pyglet.window import key

from base import AnimatedMover
from utils import load_animation

class Mouse(AnimatedMover):
    animation = load_animation("MouseRun", 6, 0.2)
    animation_left = animation.get_transform(True)
    animation_left.anchor_x = animation.anchor_x
    animation_left.anchor_y = animation.anchor_y

    def __init__(self, pos, jump_iv, gravity=24, batch=None, group=None):
        super().__init__(pos, 0, batch, group)
        self.scale = 1.5
        # Jump initial velocity
        self.jump_iv = jump_iv
        self.gravity = gravity
        self.jumpping = False

        self.pointed_right = True

    def update(self, dt, keyboard, sprite_groups, environment):
        play_ani = False
        self.vel.x = 0

        # Gravity
        self.vel.y -= self.gravity * dt

        # Collision with ground
        for env in environment:
            if self.collides_rect(env) and self.top > env.top:
                self.pos.y = env.top + self.image.anchor_y * self.scale
                self.vel.y =  0
                self.jumpping = False

        # Input handling/movement control
        if keyboard[key.W] and not self.jumpping:
            self.vel.y = self.jump_iv
            self.jumpping = True
        if keyboard[key.A]:
            self.vel.x = -80
            play_ani = True
            if self.pointed_right:
                self.image = self.animation_left
                self.pointed_right = False
        if keyboard[key.D]:
            self.vel.x = 80
            play_ani = True
            if not self.pointed_right:
                self.image = self.animation
                self.pointed_right = True

        self.paused = not (play_ani and not self.jumpping)

        # Update the player's position
        self.pos.x += self.vel.x * dt
        self.pos.y += self.vel.y * dt

        self.x = self.pos.x
        self.y = self.pos.y
