import pyglet as _pyglet

from utils import *
from vector import Vector
from base import AnimatedMover


class Projectile(AnimatedMover):
    def __init__(self, pos, angle, velocity, batch, group=None):
        super().__init__(pos, angle, batch=batch, group=group)

        self.vel.magnitude = velocity

    def update(self, dt, keyboard, sprite_group_layers):
        self.pos += self.vel * dt

        self.x = self.pos.x
        self.y = self.pos.y


class PlayerBullet(Projectile):
    animation = load_animation("MouseShipShotSpaced", 6, 0.05)
    damage = 10


class EnemyBullet(Projectile):
    animation = load_animation("CatShipShotSpaced", 6, 0.1)
    damage = 20
