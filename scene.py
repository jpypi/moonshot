import pyglet as _pyglet
from pyglet.window import key

from environment import *
from ship import Ship
from enemy import Enemy
from mouse import Mouse
from environment import Diamond, Grass, Dirt, Moon, Shuttle
from stars import StarField
from explosion import Explosion
from healthbar import BossBar
from utils import load_centered_image
import colors


FLY_UP_MSG = "Fly up to the moon!"
LOST_IN_SPACE_MSG = "You got lost in space. Press Esc to quit."
EXPLODED_MSG = "The cats win. Press Esc to quit."


def lost_in_space(player, window, bounds=100):
    return (player.x < -bounds or player.x > window.width + bounds or\
            player.y < -bounds or player.y > window.height + bounds)


def hcentered_message(message, dest_rect, y_top=100, color=(0, 0, 0, 255),
                      batch=None):
    """
    dest_rect: need only have .width and .height
    """

    document = _pyglet.text.decode_text(message)
    document.set_style(0, -1, {
        "align": "center",
        "font_name": "Roboto",
        "font_size": 20,
        "color": color })

    layout = _pyglet.text.layout.TextLayout(document, dest_rect.width,
            dest_rect.height - y_top, multiline=True, batch=batch)
    layout.anchor_x = "center"
    layout.x = dest_rect.width // 2
    layout.anchor_y = "top"
    layout.y = dest_rect.height - y_top

    return (document, layout)


class Scene:
    def __init__(self, window):
        self.window = window

        self.sprite_batch = _pyglet.graphics.Batch()
        self.sprite_group_layers = [_pyglet.graphics.OrderedGroup(i)
                                    for i in range(2, 4)]

        self.player = None
        self.bullets = []
        self.enemy_bullets = []
        self.enemies = []
        self.items = []
        self.environment = []
        self.player_hit_sound = _pyglet.media.load('sounds/PlayerHit.wav', streaming=False)
        self.pickup_sound = _pyglet.media.load('sounds/PickUP.wav', streaming=False)
        self.shuttle_enter_sound = _pyglet.media.load('sounds/ShuttleEnter.wav', streaming=False)

    def initialize(self, data=None):
        pass

    def destroy(self):
        pass

    def update(self, dt, keyboard):
        """
        return: 0 / 1 / 2 to pick scene
        """
        if self.player:
            bullets = self.player.update(dt, keyboard,
                                         self.sprite_group_layers,
                                         self.environment)
            if bullets:
                self.bullets.extend(bullets)

        remove_bullets = []
        for b in self.bullets:
            b.update(dt, keyboard, self.sprite_group_layers)

            # Remove bullets when off screen
            if b.left > self.window.width or b.right < 0 or\
                    b.bot > self.window.height or b.top < 0:
                remove_bullets.append(b)

            remove_enemies = []
            for enemy in self.enemies:
                if b.collides_rect(enemy):
                    remove_bullets.append(b)
                    enemy.health -= b.damage
                    if enemy.health <= 0:
                        Explosion(enemy.pos, 0, self.sprite_batch)
                        enemy.delete()
                        remove_enemies.append(enemy)

            for e in remove_enemies:
                self.enemies.remove(e)

        for b in remove_bullets:
            self.bullets.remove(b)
            b.delete()

        # Enemy bullets
        for enemy in self.enemies:
            bullets = enemy.update(dt, keyboard, self.sprite_group_layers, self.environment)
            if bullets:
                self.enemy_bullets.extend(bullets)

        remove_enemy_bullets = []
        for b in self.enemy_bullets:
            b.update(dt, keyboard, self.sprite_group_layers)

            # Remove bullets when off screen
            if b.left > self.window.width or b.right < 0 or\
                    b.bot > self.window.height or b.top < 0:
                remove_enemy_bullets.append(b)
            elif self.player and self.player.collides_rect(b):
                remove_enemy_bullets.append(b)
                self.player.health -= b.damage
                self.player_hit_sound.play()
                if self.player.health <= 0:
                    Explosion(self.player.pos, 0, self.sprite_batch)
                    self.player.delete()
                    self.player = None

        for b in remove_enemy_bullets:
            self.enemy_bullets.remove(b)
            b.delete()


class MessageScene(Scene):
    def initialize(self, data=None):
        super().initialize(data)
        msg = hcentered_message("", self.window, color=colors.WHITE,
                batch=self.sprite_batch)
        self._msg_doc, self._msg_layout = msg

    @property
    def message_color(self):
        self._msg_doc.get_style("color")

    @message_color.setter
    def message_color(self, value):
        self._msg_doc.set_style(0, -1, {"color": value})

    def set_message(self, message):
        self._msg_doc.text = message


class Scene1(MessageScene):
    def initialize(self, data=None):
        super().initialize(data)

        # Rendering colors
        _pyglet.gl.glClearColor(0.1, 0.1, 0.1, 1)
        self.message_color = colors.WHITE

        # Create scene objects
        self.star_field = StarField(self.window.width, self.window.height,
                                    self.sprite_batch)

        self.mouse = Mouse((150, 130), 90, 90, self.sprite_batch,
                            self.sprite_group_layers[1])

        self.ship = Ship((700, 124), 90, 0, self.sprite_batch,
                         self.sprite_group_layers[1])

        self.items.append(Diamond((600, 120), 0, self.sprite_batch))

        # Initialize the player
        self.player = self.mouse
        self.mouse_in_ship = False

        # Set up music
        self.music = _pyglet.resource.media('music/bensound-epic.mp3')
        self.music_player = self.music.play()
        self.music_player.volume = 0.2

        # Generate the ground
        x_pos = 8
        y_pos = 100
        for x in range(57):
            ground = Grass((x_pos, y_pos), self.sprite_batch,
                            self.sprite_group_layers[0])
            x_pos += ground.width
            self.environment.append(ground)

        y_pos -= 15
        for row in range(6):
            x_pos = 5
            for x in range(80):
                ground = Dirt((x_pos, y_pos), self.sprite_batch,
                                self.sprite_group_layers[0])
                x_pos += ground.width - 1
                self.environment.append(ground)
            y_pos -= self.environment[-1].height

    def update(self, dt, keyboard):
        super().update(dt, keyboard)

        if self.items:
            if self.player and self.player.collides_rect(self.items[0]):
                self.items[0].delete()
                self.items = []
                self.pickup_sound.play()

        if not self.mouse_in_ship and self.mouse.collides_rect(self.ship):
            self.mouse_in_ship = True
            self.player = self.ship
            self.mouse.delete()
            self.set_message(FLY_UP_MSG)
            self.shuttle_enter_sound.play()

        if self.player and lost_in_space(self.player, self.window):
            self.set_message(LOST_IN_SPACE_MSG)
            self.player = None

        if self.player and self.mouse_in_ship:
            self.player.vel.y -= 80 * dt
            for e in self.environment:
                if e.collides_rect(self.player) and self.player.top > e.top:
                    self.player.vel.y = 0
                    self.player.vel.x = 0

        # Advance to the next scene! (Space!)
        if self.player and self.player.bot > self.window.height:
            self.music_player.pause()
            return 1, (self.player.pos.x, self.player.vel)


class Scene2(MessageScene):
    def initialize(self, data):
        super().initialize(data)

        # Rendering colors
        _pyglet.gl.glClearColor(0.1, 0.1, 0.1, 1)
        self.message_color = colors.WHITE

        boss_health = 8000

        self.boss_bar = BossBar((10, 300), boss_health, boss_health,
                                (10, 100, 10), self.sprite_batch)
        self.boss_bar.setPos(((self.window.width - self.boss_bar.width) // 2,
                              self.window.height - self.boss_bar.height))

        self.star_field = StarField(self.window.width, self.window.height,
                                    self.sprite_batch)

        player_x = self.window.width // 2
        if data:
            player_x = data[0]
        self.player = Ship((player_x, 50), 90, 3000, self.sprite_batch,
                           self.sprite_group_layers[1])
        if data:
            self.player.vel = data[1]

        self.enemies.append(
                Enemy((self.window.width // 2, self.window.height // 2), 0,
                      boss_health,
                      self.sprite_batch, self.sprite_group_layers[1])
            )

        self.can_continue = False
        self.lost = False

        self.music = _pyglet.resource.media('music/bensound-dance.mp3')
        self.music_player = self.music.play()
        self.music_player.volume = 0.2

    def update(self, dt, keyboard):
        super().update(dt, keyboard)

        # Star field paralax
        if self.player:
            self.star_field.paralax(self.window, self.player)

        if self.enemies:
            self.boss_bar.update(self.enemies[0].health)
        elif self.boss_bar:
            self.boss_bar.delete()
            self.boss_bar = None
            self.can_continue = True
            self.set_message(FLY_UP_MSG)

        if self.player and lost_in_space(self.player, self.window):
            self.set_message(LOST_IN_SPACE_MSG)
            self.player = None
            self.lost = True

        if self.player is None and not self.lost:
            self.set_message(EXPLODED_MSG)

        if self.player and self.can_continue and \
                self.player.bot > self.window.height:
            self.music_player.pause()
            return 2


class Scene3(MessageScene):
    def initialize(self, data=None):
        super().initialize(data)
        _pyglet.gl.glClearColor(0.1, 0.1, 0.1, 1)
        self.message_color = colors.WHITE
        self.set_message("Congratulations you made it to the moon! Press Esc to exit.")

        self.star_field = StarField(self.window.width, self.window.height,
                                    self.sprite_batch)

        self.player = Mouse((275, 390), 90, 90, self.sprite_batch,
                            self.sprite_group_layers[1])

        #self.ship = Ship((200, 300), 90, 0, self.sprite_batch,
                        # self.sprite_group_layers[1])

        self.moon = Moon((300, 300), self.sprite_batch,
                            self.sprite_group_layers[0])

        self.shuttle_scaffold = Shuttle((200,400),
                            self.sprite_batch, self.sprite_group_layers[1])

        self.environment.append(self.moon)
        self.environment.append(self.shuttle_scaffold)

        self.player.gravity = 0

        self.music = _pyglet.resource.media('music/bensound-summer.mp3')
        self.music_player = self.music.play()
        self.music_player.volume = 0.2

    def update(self, dt, keyboard):
        super().update(dt, keyboard)

        # Star field paralax
        self.star_field.paralax(self.window, self.player)
