import pyglet as _pyglet
from pyglet.window import key

from utils import *
from vector import Vector
from projectiles import PlayerBullet
from healthbar import BasicBar
from base import RectSprite


class Ship(RectSprite):
    animation,static = load_animation("MouseShip3", 3, 0.1, ani=2)

    def __init__(self, pos, angle, health, batch=None, group=None):
        super().__init__(self.static[0], pos[0], pos[1],
                         batch=batch, group=group)

        self.pos = Vector(pos[0], pos[1], True)
        self.vel = Vector(0, 0, True)

        self.angle = angle

        self.max_speed = 190
        self.bullet_speed = 450
        # Bullets per second
        self.fire_rate = 8
        # initialize this to just something bigger than rate
        self.time_since_last_bullet = 1/self.fire_rate

        self.mouse_weapon_sound = _pyglet.media.load('sounds/MouseWeapon.wav', streaming=False)
        self.mouse_boost_sound = _pyglet.media.load('sounds/boost.wav', streaming=False)
        self.sound_rate = 2
        self.time_since_last_sound = 1/self.sound_rate

        self.health = health
        self.health_bar = None
        if self.health:
            health_bar_pos = (self.x - self.width // 2,
                              self.y + self.height // 2 + 10)
            self.health_bar = BasicBar(health_bar_pos, self.width,
                    self.health, self.health,
                    (10, 100, 10), (150, 150, 150), batch, group)

    @property
    def angle(self):
        return self._angle

    @angle.setter
    def angle(self, value):
        self._angle = value
        self.rotation = 360 - (value - 90)
        if self.rotation > 360:
            self.rotation -= 360
        if self.rotation < 0:
            self.rotation += 360

    def update(self, dt, keyboard, sprite_groups, environment):
        # Degrees / sec
        rot_rate = 200 * dt
        self.time_since_last_sound += dt
        acc_vec = Vector(200, self.angle)

        if keyboard[key.W]:
            self.vel += acc_vec * dt
            if not isinstance(self.image, _pyglet.image.Animation):
                self.image = Ship.animation
                self.paused = False
            if self.time_since_last_sound > 1 / self.sound_rate:
                self.time_since_last_sound = 0
                self.mouse_boost_sound.play()
        else:
            self.image = self.static[0]

        if keyboard[key.A]:
            self.angle += rot_rate
        if keyboard[key.D]:
            self.angle -= rot_rate

        self.pos.x += self.vel.x * dt
        self.pos.y += self.vel.y * dt

        self.x = self.pos.x
        self.y = self.pos.y

        if self.health_bar:
            self.health_bar.update(self.health)
            health_bar_pos = (self.x - self.width // 2,
                              self.y + self.height // 2 + 10)
            self.health_bar.setPos(health_bar_pos)

        self.time_since_last_bullet += dt
        if keyboard[key.SPACE] and self.time_since_last_bullet > 1 / self.fire_rate:
            self.time_since_last_bullet = 0
            shift_forward = Vector(10, self.angle)
            start_pos1 = self.pos + shift_forward + Vector(21, self.angle + 90)
            start_pos2 = self.pos + shift_forward + Vector(21, self.angle - 90)
            projectiles = (
                    PlayerBullet(start_pos1, self.angle - 90, self.bullet_speed,
                               self.batch, sprite_groups[0]),
                    PlayerBullet(start_pos2, self.angle - 90, self.bullet_speed,
                               self.batch, sprite_groups[0])
                )
            self.mouse_weapon_sound.play()
            return projectiles

