from itertools import cycle
import random

import pyglet as _pyglet


class StarField:
    def __init__(self, width, height, sprite_batch):
        self.layers = [
                self.createLayer(width, height, 400, sprite_batch),
                self.createLayer(width, height, 300, sprite_batch),
                self.createLayer(width, height, 200, sprite_batch)
            ]
        self.layer_shifts = [
                 [0, 0],
                 [0, 0],
                 [0, 0]
                ]

    @staticmethod
    def createLayer(width, height, n, batch):
        c = cycle((width, height))
        vertex_list = batch.add(n, _pyglet.gl.GL_POINTS, None, 'v2i/dynamic')

        for i in range(2 * n):
            vertex_list.vertices[i] = random.randrange(0, next(c))

        return vertex_list

    def paralax(self, window, player):
        delta_x = player.x - window.width // 2
        delta_y = player.y - window.height // 2
        scale_fact_1 = 8
        scale_fact_2 = 50
        self.shift(-delta_x // scale_fact_1, -delta_y // scale_fact_1, -1)
        self.shift(-delta_x // scale_fact_2, -delta_y // scale_fact_2, -2)

    def shift(self, slide_x, slide_y, layer=-1):
        slide = (int(slide_x), int(slide_y))

        shifted = self.layer_shifts[layer]

        delta = (slide[0] - shifted[0], slide[1] - shifted[1])

        for i, p in enumerate(self.layers[layer].vertices):
            self.layers[layer].vertices[i] = p + delta[i % 2]

        self.layer_shifts[layer] = slide
