import os

import pyglet as _pyglet


def load_resource(path):
    return os.sep.join(("images", f"{path}.png"))


def load_centered_image(image):
    image = _pyglet.image.load(load_resource(image))
    image.anchor_x = image.width // 2
    image.anchor_y = image.height // 2

    return image


def load_image_grid(resource_name, length, rows=1):
    image = _pyglet.image.load(load_resource(resource_name))
    image_seq = _pyglet.image.ImageGrid(image, rows, length)
    for img in image_seq:
        img.anchor_x = img.width // 2
        img.anchor_y = img.height // 2

    return image_seq


def load_animation(animation, length, period, rows=1, ani=None):
    image_seq = load_image_grid(animation, length, rows)
    if ani is not None:
        animation = _pyglet.image.Animation.from_image_sequence(image_seq[:ani],
                                                                period)
        animation.anchor_x = image_seq[0].anchor_x
        animation.anchor_y = image_seq[0].anchor_y
        return (animation, image_seq[ani:])
    else:
        animation = image_seq.get_animation(period)
        animation.anchor_x = image_seq[0].anchor_x
        animation.anchor_y = image_seq[0].anchor_y
        return animation
